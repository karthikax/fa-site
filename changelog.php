<!DOCTYPE html>
<html>
<head>
	<title>Firebase Admin - Changelog</title>
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta property="og:url"         content="https://firebaseadmin.com" />
	<meta property="og:type"        content="website" />
	<meta property="og:title"       content="Firebase Admin - Firebase Management Tool" />
	<meta property="og:description" content="Download different versions and formats of Firebase Admin including windows, mac and linux for 32bit 64bit architectures." />
	<meta property="og:image"       content="https://firebaseadmin.com/screenshot.png" />
	<meta name="theme-color"        content="#faba61" />
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-45866404-3', 'auto');
		ga('send', 'pageview');
	</script>
	<link rel="shortcut icon" href="https://s9.postimg.org/f7i558jjj/favicon.png">
	<link rel="preload" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,500italic,700,700italic|Roboto+Mono:400,500,700|Material+Icons" as="style" onload="this.rel='stylesheet'">
	<style type="text/css">
html,body,h1,h2,h3,ul,ol{
	margin: 0;
	padding: 0;
}
html,body{
	height: 100%;
	color: #333;
	font: 400 16px Roboto,sans-serif;
	line-height: 1.2;
	-moz-osx-font-smoothing: grayscale;
	-webkit-font-smoothing: antialiased;
	-webkit-text-size-adjust: 100%;
	-ms-text-size-adjust: 100%;
	text-size-adjust: 100%;
}
h1, h2, h3{
	font-weight: 300;
}
a, a:visited{
	text-decoration: none;
	color: inherit;
}
section {
	overflow: hidden;
	border-bottom: 1px solid #eee;
}
#home{
	position: relative;
	height: 36px;
	color: #fff;
	overflow: hidden;
}
section h2{
	font-size: 28px;
	font-weight: 300;
	padding-top: 0;
	padding-bottom: 5px;
	margin-bottom: 10px;
	border-bottom: 1px solid #ddd;
}
header{
	display: flex;
	flex-direction: row;
	padding: 0 40px;
	position: fixed;
	left: 0;
	right: 0;
	z-index: 999;
	transition: all .4s;
}
header.scroll{
	background: rgba(255, 255, 255, 0.97);
	color: #f89d40;
	box-shadow: 0 0 2px rgba(0, 0, 0, .2);
}
#logo{
	font-size: 24px;
	font-weight: 400;
    line-height: 2.4;
    transition: all .4s;
}
#logo span {
    padding-left: 20px;
    color: #666;
}

#changelog{
	padding: 40px;
	max-width: 800px;
	margin: auto;
	min-height: 60%;
}
.release{
	padding-bottom: 20px;
}
.release ul{
	padding-left: 20px;
}
.release .download{
	float: right;
	color: #4078c0;
	font-size: 16px;
	line-height: 2.4;
}

@media (min-width: 768px) {
	header{
		padding: 0 20px;
	}
	.scroll #logo{
		font-size: 18px;
		line-height: 2;
	}
}
@media (min-width: 1024px) {
	header{
		padding: 0 20px;
	}
}
	</style>
</head>
<body>
	<section id="home">
		<header class="scroll">
			<h1 id="logo"><a href="/">Firebase Admin</a> <span>Changelog</span></h1>
		</header>
	</section>
	<section id="changelog">
		<?php

		@include_once('info.php');

		foreach ($changes as $version => $change_list) {
			$version_class = str_replace('.', '', $version);
			echo '<div class="release">';
			echo "<h2>Version $version <a href='releases#release$version_class' class='download'>Download</a></h2>";
			echo '<ul>';
			foreach ($change_list as $change) {
				echo "<li>$change</li>";
			}
			echo '</ul>';
			echo '</div>';
		}

		?>
	</section>
	<footer>
		<ul>
			<li><a href="/">Home</a></li>
			<li><a href="//github.com/codefoxes/firebase-admin" target="_blank">GitHub</a></li>
			<li><a href="changelog.html">Changelog</a></li>
		</ul>
		<div class="copyright">
			&copy; 2016 &middot; Codefoxes &middot; All Rights Reserved
		</div>
	</footer>
</body>
<style type="text/css">
	img{
		max-width: 100%;
	}
	.share{
		padding-top: 8%;
	}
	.share .buttons{
		text-align: center;
		padding: 30px 0 20px;
	}
	.share .button{
		background: #3b5998;
		display: inline-block;
		width: 50px;
		height: 50px;
		border-radius: 25px;
		padding-top: 12px;
		box-sizing: border-box;
		margin: 0 20px;
	}
	.button.google{
		background: #DC4E41;
	}
	.button.twitter{
		background: #55ACEE;
	}
	.share svg{
		width: 24px;
		height: 24px;
	}
	footer{
		background: #37424b;
		padding: 40px 0;
		overflow: auto;
		color: #cfd8dc;
		text-align: center;
	}
	footer ul{
		display: flex;
		float: left;
		list-style: none;
	}
	footer a{
		display: block;
		padding: 10px 20px;
	}
	.copyright{
		float: right;
		padding: 10px 0;
	}
	@media (min-width: 768px) {
		footer{
			padding: 40px;
		}
	}
</style>
<script type="text/javascript">
var url = encodeURIComponent(document.URL);
var text = encodeURIComponent(document.title);

function socialShare(site) {
	var link;
	switch (site) {
		case 'facebook':
			link = '//www.facebook.com/sharer/sharer.php?u=' + url + '&t=' + text;
			break;
		case 'google':
			link = '//plus.google.com/share?url=' + url;
			break;
		case 'twitter':
			link = '//twitter.com/share?text=' + text;
			break;
	}
	window.open(link, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
}

</script>
</html>