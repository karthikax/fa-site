<?php
$count = @file_get_contents('../count.txt');
if (!$count || $count <= 0) {
    $count = 1;
}
header('Content-type: image/svg+xml');
echo '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
?>
<svg xmlns="http://www.w3.org/2000/svg" width="110" height="20">
    <linearGradient id="b" x2="0" y2="100%">
        <stop offset="0" stop-color="#bbb" stop-opacity=".1"/>
        <stop offset="1" stop-opacity=".1"/>
    </linearGradient>
    <mask id="a">
        <rect width="110" height="20" rx="3" fill="#fff"/>
    </mask>
    <g mask="url(#a)">
        <rect width="71" height="20" fill="#555"/>
        <rect x="71" width="39" height="20" fill="#007ec6"/>
        <rect width="110" height="20" fill="url(#b)"/>
    </g>
    <g fill="#fff" text-anchor="middle" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="11">
        <text x="36.5" y="15" fill="#010101" fill-opacity=".3">downloads</text>
        <text x="36.5" y="14">downloads</text>
        <text x="89.5" y="15" fill="#010101" fill-opacity=".3"><?php echo $count; ?></text>
        <text x="89.5" y="14"><?php echo $count; ?></text>
    </g>
</svg>
