<?php

@include_once('info.php');

$toupdate = 0;

if (isset($_GET['v'])) {
	$toupdate = version_compare($current_version, $_GET['v']);
}

function check_update() {
	global $toupdate;
	if ($toupdate > 0) {
		return [
			'update' => true,
			'message' => 'New version is available!'
		];
	} else {
		return [
			'update' => false,
			'message' => 'Your Firebase admin is upto date!'
		];
	}
}

$result = check_update();

if (isset($_SERVER['HTTP_RESPONSE_FORMAT']) && $_SERVER['HTTP_RESPONSE_FORMAT'] == 'json') {
	$res = [
		'message' => $result['message'],
		'version' => $current_version
	];
	if ($result['update']) {
		$res['update'] = true;
		$res['changes'] = [
			'Updater added'
		];
	}
	header('Content-type: application/json');
	echo json_encode($res);
} else {
	$message = $result['message'];
	if ($result['update']) {
		$message .= '<br><a href="/#download">Download</a>';
	}
	echo $message;
}
